#!/bin/bash

set -e
set -u

repo_base_url="git@github.com:OLIMEX"
repo_list=""
repo_list="$repo_list iCE40HX1K-EVB"
repo_list="$repo_list iCE40HX8K-EVB"
repo_list="$repo_list iCE40-IO"
repo_list="$repo_list iCE40-DIO"
repo_list="$repo_list iCE40-ADC"
repo_list="$repo_list iCE40-DAC"
git_clone_opts="--depth 1"

for repo_name in $repo_list; do
	git clone $git_clone_opts $repo_base_url/${repo_name}.git
done
