# OLIMEX iCE40HX8K-EVK

The repository we maintain documentation and examples for the [OLIMEX iCE40HX8K-EVK](https://www.olimex.com/Products/FPGA/iCE40/iCE40HX8K-EVB/open-source-hardware).

Run the fetch script to obtain the repositories provided by OLIMEX.

```
./fetch.sh
```

## Programmer

The programmer we use is a [OLIMEXINO-32U4](https://www.olimex.com/Products/Duino/AVR/OLIMEXINO-32U4/open-source-hardware).

Outline of what needs to be done:

 * program OLIMEXINO with iCD40 programmer sketch using Arduino
 * build and install iceprogduino utility
 * get the tty device filename for the programmer
 * test iceprogduino with OLIMEXINO

See ```iCE40HX1K-EVB/programmer/olimexino-32u4 firmware/README.md```
for instructions on how to turn the OLIMEXINO into a programmer for the iCE40.

To build and install the iceprogduino executable:

```
make -C iCE40HX1K-EVB/programmer/iceprogduino
sudo install -m 0755 iCE40HX1K-EVB/programmer/iceprogduino/iceprogduino /usr/local/bin/
```

Now, identify the device filename for the programmer by running:

```
ls -al /dev/serial/by-id
```

Note the number of the ttyACM file where the name of the symlink is ```usb-Arduino_LLC_Olimexino-32U4-if00```.
In my case it was ```/dev/ttyACM1``` because I have a built-in modem in my machine.

To test iceprogduino can talk to the OLIMEXINO board, we have to identity the tty device
file that represents the programer hardware.

```
iceprogduino -I /dev/ttyACM1 -t
```

Should result in output like this:

```

Test mode
Serial: /dev/ttyACM1: Success
Bye.
```

## Toolchain

See the [icestorm-toolchain](https://gitlab.com/Lazlo/icestorm-toolchain)
repository for a script to automatically build the required toolchain.
It consists of IceStorm tools, Yosis and Arachne-pnr.

## Demos

```
make -C iCE40HX8K-EVB/demo/ice40hx8k-evb/ prog
```

> NOTE If the device filename for the programmer is not ```/dev/ttyACM0```, make sure
> to modify the Makefile and add ```-I /dev/ttyACM<n>``` to calls of iceprogduino.
